import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link, Switch, Redirect } from "react-router-dom";
import './App.css';

class CreateUser extends Component {
    constructor(props) {
        super(props);
    }


    componentDidMount() {

    }

    render() {
        return (
            <div>
                <h1>Create User</h1>
                <center>
                    <table>
                        <tbody>
                        <tr>
                            <td>
                                <input ref={input => this.login = input} placeholder="login" onChange={this.loginOnChange} />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input ref={input => this.password = input} type="password" placeholder="password" onChange={this.passwordOnChange} />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button type='submit' onClick={this.createuser}>Create</button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <label ref={label => this.label = label}></label>
                </center>
            </div>
        );
    }

    createuser = (e) => {
        this.props.createUser(this.login.value, this.password.value).then(message => {
            if (message.msg === "username and password missing")
            {
                this.label.innerHTML = message.msg
            }
            else if(message.msg === "password missing")
            {
                this.label.innerHTML = message.msg
            }
            else if(message.msg === "username allready taken")
            {
                this.label.innerHTML = message.msg
            }
            else if(message.msg === "User created")
            {
                this.label.innerHTML = message.msg
            }
            else
            {
                this.label.innerHTML = "An error happened";
            }
        })

    }
}

export default CreateUser;