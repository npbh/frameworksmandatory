import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link, Switch, Redirect } from "react-router-dom";
import './App.css';

class AddQuestion extends Component {
    constructor(props) {
        super(props);
        this.state = {topic: '', text: ''}
    }


    componentDidMount() {

    }

    render() {
        if(!this.props.auth.loggedIn())
        {
                return (<Redirect to='/login' />)
        }

        return (
            <div>
                <br/>
                <input placeholder="topic" onChange={this.topicOnChange} className="answer-text" /><br/>
                <textarea placeholder="text" onChange={this.bodyOnChange} className="answer-textarea" /><br/>
                <button className="answer-button" onClick={this.handleSubmit}>Add Question</button>
            </div>
        );
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.addquestion({topic: this.state.topic, text: this.state.text})
        this.props.history.push('/')
    }

    topicOnChange = (e) => {
        this.setState({topic: e.target.value})
    }

    bodyOnChange = (e) => {
        this.setState({text: e.target.value})
    }
}

export default AddQuestion;