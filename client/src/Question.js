import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import './App.css';
import AddAnswer from "./AddAnswer";

class Question extends Component {
    state = {"editquestion": false, "editquestioninserted": false, "editAnswer": "", "editAnswerInserted": false };

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.method(this.props.id)
    }

    editAnswer = (e) => {
        if (this.state.editAnswer != "")
        {

            this.props.updateAnswer(this.props.id, this.state.editAnswer, {"topic": this.topic.value, "text": this.text.value})
            this.setState({"editAnswer": ""});
        }
        else
        {
            this.setState({"editAnswer": e.target.id});
            this.setState({"editquestion": false})
        }
    }

    editQuestion = () => {
        if (this.state.editquestion)
        {
            this.setState({"editquestion": false});
            this.props.updateQuestion(this.props.id, {topic: this.topic.value, text: this.text.value})
            this.setState({"editquestioninserted": false})
        }
        else
        {

            this.setState({"editquestion": true});
            this.setState({"editAnswer": ""});
        }

    }

    render() {
        const { question } = this.props.Question;
        let loggedin = this.props.Auth.loggedIn();
        let userinfo = this.props.Auth.getUserInfo();
        let answers = [];
        let addanswer = loggedin ? <AddAnswer addAnswer={this.props.addAnswer} id={this.props.id} method={this.props.method} /> : null;

        let questionedit = <li>{this.state.editquestion ? <textarea ref={text => this.text = text} className="txtEditArea" defaultValue={question.text}/> : question.text}<br/>{loggedin && question.username === userinfo.username && !this.state.editAnswer ? <button onClick={this.editQuestion} className="editbutton">{this.state.editquestion ? "Save" : "Edit"}</button> : null}</li>;
        let questiontopic = this.state.editquestion ? <input className="txtEdit" ref={topic => this.topic = topic} defaultValue={question.topic} /> : question.topic;

        if (question.answers)
        {
            question.answers.forEach((answer, index) => {

                answers.push(<li key={index + 'header'} className="answer-header"><ul>
                    <li className="created">{answer.created}</li>
                    <li className="username">{answer.username}</li>
                    <li className="votes">Votes: {answer.votes}</li>
                    </ul>
                    </li>
                )

                let answertopic = this.state.editAnswer === answer._id ? <input ref={topic => this.topic = topic} className="txtEdit" defaultValue={answer.topic} /> : answer.topic;
                let answertext  = this.state.editAnswer === answer._id ? <textarea ref={text => this.text = text} className="txtEditArea" defaultValue={answer.text} /> : answer.text;

                answers.push(<li key={index + 'topic'} className="answer-topic"><ul><li className="question-title">{answertopic}</li></ul></li>);
                    answers.push(<li key={index + 'forumanswer'} className="forum-answer">
                        <ul>
                            <li>{answertext}<br/>{loggedin && answer.username !== userinfo.username ? <button id={answer._id} onClick={this.vote} className="votebutton">Vote</button> : null}{loggedin && answer.username === userinfo.username && (answer._id === this.state.editAnswer || this.state.editAnswer === "" ) && !this.state.editquestion ? <button id={answer._id} onClick={this.editAnswer} className="editbutton">{this.state.editAnswer !== "" && this.state.editAnswer === answer._id.toString() ? "Save" : "Edit"} </button> : null}</li>
                        </ul>
                    </li>);
            })
        }

        return (
            <div className="questions">
                <ul className="forum">
                    <li className="question-header"><ul>
                        <li className="created">{question.created}</li>
                        <li className="username">{question.username}</li>
                    </ul>
                    </li>
                    <li className="answer-topic">
                        <ul>
                            <li className="question-title">{questiontopic}</li>
                        </ul>
                    </li>
                    <li className="forum-question">
                        <ul>
                            {questionedit}
                        </ul>
                    </li>
                    <li className="forum-answers">
                        <ul>
                        {
                            answers
                        }
                        </ul>
                    </li>
                </ul>
                {addanswer}
            </div>
        );
    }

    vote = (e) => {
        this.props.addVote(this.props.id, e.target.id);
    }
}

export default Question;