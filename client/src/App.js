import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link, Switch, Redirect } from "react-router-dom";
import './App.css';
import Questions from "./Questions";
import Question from "./Question";
import Login from "./Login";
import AuthService from './AuthService';
import AddQuestion from "./AddQuestion";
import Profile from "./Profile";
import CreateUser from "./CreateUser";

class App extends Component {
    // Initialize state
    state = { questions: [], question: '', loggedIn: false}

    API_URL = '/api';

    constructor(props) {
        super(props);
        this.Auth = new AuthService(`${this.API_URL}/users/authenticate`);
    }

    componentDidMount() {
        this.setState({loggedIn: this.Auth.loggedIn()})
    }


    changeLoginStatus = () => {
        this.setState({loggedIn: !this.state.loggedIn})
    }


    getQuestions = () => {
            fetch('/api/questions')
                .then(res => res.json())
                .then(questions => this.setState({ questions }));
    }

    getQuestion = (id) => {
        fetch('/api/questions/' + id)
            .then(res => res.json())
            .then(questions => this.setState({ question: questions }));
    }

    addQuestion = (message) => {
        this.Auth.fetch(`${this.API_URL}/questions/`,{
            method: "POST",
            body: JSON.stringify(message)
        })
    }

    updateQuestion = (id, message) => {
        this.Auth.fetch(`${this.API_URL}/questions/` + id,{
            method: "PUT",
            body: JSON.stringify(message)
        }).then(() => {
            this.getQuestion(id)
        })
    }

    addAnswer = (id, message) => {
        this.Auth.fetch(`${this.API_URL}/questions/` + id,{
            method: "POST",
            body: JSON.stringify(message)
        }).then(() => {
            this.getQuestion(id)
        })
    }

    addVote = (questionid, answerid) => {
        this.Auth.fetch(`${this.API_URL}/questions/${questionid}/${answerid}/vote`,{
            method: "PUT",
        }).then(() => {
            this.getQuestion(questionid)
        })
    }

    updateAnswer = (questionid, answerid, message) => {
        this.Auth.fetch(`${this.API_URL}/questions/` + questionid + '/' + answerid,{
            method: "PUT",
            body: JSON.stringify(message)
        }).then(() => {
            this.getQuestion(questionid)
        })
    }

    createUser = (login, password) => {
        let body = {username: login, password: password};
        return Promise.resolve(fetch('/api/users',{
            method: "post",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify(body)
        }).then((res) => res.json()))
    }

    render() {
        let loggedin = this.Auth.loggedIn() ? <Link to={'/Profile'}>{this.Auth.getUserInfo().username}</Link> : <><Link to={'/Login'}>Login</Link> - <Link to={'/CreateUser'}>Register</Link></>;


        return (
            <Router>
            <div className="App">
                <header>
                    <h1>Q&A</h1>
                </header>
                <menu>
                    <Link to={'/'}>Forside</Link> - {loggedin}
                </menu>
                <main>
                    <Route exact path ="/"
                           render={(props) => <Questions {...props} method={this.getQuestions}  Questions={this.state} Auth={this.Auth} />}
                    />

                    <Route path ="/Question/:id"
                           render={(props) => <Question {...props} method={this.getQuestion} Question={this.state} id={props.match.params.id} addAnswer={this.addAnswer} Auth={this.Auth} addVote={this.addVote} updateQuestion={this.updateQuestion} updateAnswer={this.updateAnswer} />}
                    />

                    <Route path ="/Login"
                           render={(props) => <Login {...props} auth={this.Auth} state={this.state} changeLoginStatus={this.changeLoginStatus} />}
                    />

                    <Route path ="/Profile"
                           render={(props) => <Profile {...props} auth={this.Auth} state={this.state} changeLoginStatus={this.changeLoginStatus} />}
                    />

                    <Route path ="/AddQuestion"
                           render={(props) => <AddQuestion {...props} auth={this.Auth} addquestion={this.addQuestion} />}
                    />

                    <Route path ="/CreateUser"
                           render={(props) => <CreateUser {...props} auth={this.Auth} createUser={this.createUser} />}
                    />
                </main>
            </div>
            </Router>
        );
    }
}

export default App;