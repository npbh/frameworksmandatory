import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link, Switch, Redirect } from "react-router-dom";
import './App.css';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {login: '', password: '', loggedIn: false, redirect: false}
        this.login = this.login.bind(this);
        this.logout = this.logout.bind(this);
    }


    componentDidMount() {
        this.setState({loggedIn: this.props.auth.loggedIn()})
    }

    setRedirect = () => {
        this.setState({
            redirect: true
        })
    }

    renderRedirect = () => {
        if (this.state.redirect) {
            return <Redirect to='/' />
        }
    }

    render() {
        let div;
        if (this.props.state.loggedIn)
        {
            div = <div><h1>Logged in</h1><button onClick={this.logout}>Logout</button></div>;
        }
        else
        {
            div = <div>
                <h1>Login</h1>
                <center>
                    <table>
                        <tbody>
                        <tr>
                            <td>
                                <input onChange={this.loginOnChange} />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input onChange={this.passwordOnChange} />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button type='submit' onClick={this.login}>Login</button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </center>
            </div>;
        }

        return (
            <div>
                {this.renderRedirect()}
                {div}
            </div>
        );
    }

    login(e){
        e.preventDefault();
        this.props.auth.login(this.state.login,this.state.password)
            .then(response => {
                console.log(response)
                if (response.msg === 'User authenticated successfully')
                {
                    this.props.changeLoginStatus();
                    this.setRedirect();
                }
                else
                {
                    console.log(response)
                }
            })

    }

    logout(e){
        this.props.auth.logout();
        this.props.changeLoginStatus();
        this.setRedirect();
    }

    loginOnChange = (e) => {
        this.setState({login: e.target.value})
    }

    passwordOnChange = (e) => {
        this.setState({password: e.target.value})
    }
}

export default Login;