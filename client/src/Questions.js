import React, { Component } from 'react';
import {BrowserRouter as Router, Route, Link, Switch, NavLink} from "react-router-dom";
import './App.css';

class Questions extends Component {
    componentDidMount() {
        this.props.method();
    }

    render() {
        const { questions } = this.props.Questions;
        let addquestion = this.props.Auth.loggedIn() ? <h2><Link to={'/AddQuestion'}>Ask question</Link></h2> : null;
        return (
            <div className="questions">
                <ul className="forum">
                    <li className="forum-header">
                        <ul>
                            <li className="title">Topic</li>
                            <li className="username">Username</li>
                            <li className="answers">Answers</li>
                            <li className="created">Created</li>
                        </ul>
                    </li>
                    <li className="forum-body">
                    {
                        questions.map((question, index) =>
                            <ul key={index}>
                                <li className="title"><Link to={'/Question/' + question._id}> {question.topic}</Link></li>
                                <li className="username"><span>{question.username}</span></li>
                                <li className="answers"><span>{question.answers.length}</span></li>
                                <li className="created"><span>{question.created}</span></li>
                            </ul>
                        )
                    }
                    </li>
                </ul>
                {addquestion}
            </div>
        );
    }
}

export default Questions;