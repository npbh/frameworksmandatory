import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import './App.css';

class AddAnswer extends Component {
    constructor(props) {
        super(props);
        this.state = {topic: '', text: ''}
    }


    componentDidMount() {

    }

    render() {
        return (
            <div>
                <input ref={input => this.topic = input}  placeholder="topic" className="answer-text" onChange={this.topicOnChange} /><br/>
                <textarea ref={input => this.text = input} placeholder="text" className="answer-textarea" onChange={this.bodyOnChange} /><br/>
                <button className="answer-button" onClick={this.handleSubmit}>Add Answer</button>
            </div>
        );
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.addAnswer(this.props.id,{topic: this.state.topic, text: this.state.text})
        this.topic.value = "";
        this.text.value = "";
    }

    topicOnChange = (e) => {
        this.setState({topic: e.target.value})
    }

    bodyOnChange = (e) => {
        this.setState({text: e.target.value})
    }
}

export default AddAnswer;