module.exports = (mongoose) => {
    let express = require('express');
    let router = express.Router();

    let answerSchema = mongoose.Schema({
        username: String,
        topic: String,
        text: String,
        created: String,
        updated: String,
        votes: Number
    })

    let questionSchema = mongoose.Schema({
        username: String,
        topic: String,
        text: String,
        answers: [answerSchema],
        created: String,
        updated: String
    });


    let User = require('mongoose').model("User");
    let Question = mongoose.model("Question", questionSchema);


    //get all questions
    router.get('/', (req, res) => {
        Question.find((err, questions) => {
            console.log("get questions");
            if (err) return console.error(err);
            res.json(questions);
        })
    });

    //get specific question by id
    router.get('/:id', (req, res) => {
        Question.findById(req.params.id, (err, results) => {
            console.log("get question with id: " + req.params.id);
            try {
                res.json(results);
            } catch (error) {
                console.log("errror getting results")
                res.json(error)
            }
        });
    })

    //add new question
    router.post('/', (req, res) => {
        let date = new Date();
        date.setUTCHours(date.getUTCHours() + 2)
        let datestring = date.getDate() + '-' + (date.getUTCMonth() + 1) + '-' + date.getUTCFullYear() + ' ' + date.getUTCHours() + ':' + date.getUTCMinutes() + ':' + date.getUTCSeconds()
            let newQuestion = new Question();
            newQuestion.username = req.user.username;
            newQuestion.topic = req.body.topic;
            newQuestion.text = req.body.text;
            newQuestion.created = datestring;
            newQuestion.save();
            res.json("Question added");
    });

    //update question
    router.put('/:id', (req, res) => {
        let date = new Date();
        date.setUTCHours(date.getUTCHours() + 2)
        let datestring = date.getDate() + '-' + (date.getUTCMonth() + 1) + '-' + date.getUTCFullYear() + ' ' + date.getUTCHours() + ':' + date.getUTCMinutes() + ':' + date.getUTCSeconds()
        User.findOne({username: req.user.username}).then(function (user) {
            Question.findById(req.params.id, (err, question) => {
                try {
                    if (question.username === user.username) {
                        question.topic = req.body.topic;
                        question.text = req.body.text;
                        question.updated = datestring;
                        question.save()
                        console.log(`Updated question`);
                        console.log(question);
                        res.json({msg: "Question updated", task: question});
                    } else {
                        return res.status(401).send('unauthorized');
                    }
                } catch (error) {
                    console.log("errror getting results")
                    res.json(error)
                }
            });
        });
    });

    //add answer to question
    router.post('/:id', (req, res) => {
        Question.findById(req.params.id, (err, question) => {
            let date = new Date();
            date.setUTCHours(date.getUTCHours() + 2)
            let datestring = date.getDate() + '-' + (date.getUTCMonth() + 1) + '-' + date.getUTCFullYear() + ' ' + date.getUTCHours() + ':' + date.getUTCMinutes() + ':' + date.getUTCSeconds()
            question.answers.push({username: req.user.username, topic: req.body.topic, text: req.body.text, created: datestring, votes: 0})
            question.save();
            res.json("answer added");
        });
    });

    //update answer
    router.put('/:id/:answerid', (req,res) => {
        User.findOne({username: req.user.username}).then(function (user) {
            Question.findById(req.params.id, (err, question) => {
                let doc = question.answers.id(req.params.answerid);
                if (doc.username === user.username)
                {
                    let date = new Date();
                    date.setUTCHours(date.getUTCHours() + 2)
                    let datestring = date.getDate() + '-' + (date.getUTCMonth() + 1) + '-' + date.getUTCFullYear() + ' ' + date.getUTCHours() + ':' + date.getUTCMinutes() + ':' + date.getUTCSeconds()
                    doc.text = req.body.text;
                    doc.topic = req.body.topic;
                    doc.updated = datestring;
                    question.save();
                    res.json("answer updated");
                }
                else
                {
                    return res.status(401).send('unauthorized');
                }
            });
        });
    });

    //vote on answer
    router.put('/:id/:answerid/vote', (req,res) => {
        console.log("vote");
        Question.findById(req.params.id, (err, question) => {
            let doc = question.answers.id(req.params.answerid)
            ++doc.votes;
            question.save();
            res.json("vote added");
        });
    })

    return router;
};