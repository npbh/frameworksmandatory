module.exports = (mongoose) => {
    let express = require('express');
    let router = express.Router();
    const jwt = require('jsonwebtoken');
    const bcrypt = require('bcrypt');

    let userSchema = mongoose.Schema({
        username: String,
        password: String
    })

    let User = mongoose.model("User", userSchema);

    router.post('/', (req, res) => {
        if (!req.body.password && !req.body.username)
        {
            res.json({msg: "username and password missing"})
        }
        else if (!req.body.password)
        {
            res.json({msg: "password missing"})
        }
        else if(!req.body.username)
        {
            res.json({msg: "username missing"})
        }
        else
        {
        let newUser = new User();
        User.find({username: req.body.username}, (err, result) => {
            if (result.length > 0)
            {
                res.json({msg: "username allready taken"});
            }
            else
            {
                bcrypt.hash(req.body.password, 10, function(err, hash) {
                    if (err)
                    {
                        res.json({msg: "Error " + err})
                    }
                    else
                    {
                        newUser.username = req.body.username;
                        newUser.password = hash;
                        newUser.save();
                        res.json({msg: "User created"});
                    }
                });
            }

        })

    }});

    router.put('/', (req, res) => {
        let password = req.body.password;
        let newpassword = req.body.newpassword;

        if (password)
        {
            User.findOne({ username: req.user.username}, (err, user) => {
                if (user) {
                    bcrypt.compare(password, user.password, (err, result) => {
                        if (result) {
                            if (newpassword)
                            {
                                bcrypt.hash(newpassword, 10, function(err, hash) {
                                    if (err)
                                    {
                                        res.json({msg: "Error " + err})
                                    }
                                    else
                                    {
                                        user.password = hash;
                                        user.save();
                                        res.json({msg: "userinfo updated"});
                                    }
                                });
                            }
                            else
                            {
                                user.save();
                                res.json("userinfo updated");
                            }
                        }
                        else res.status(401).json({msg: "Password mismatch!"})
                    });
                } else {
                    res.status(404).json({msg: "User not found!"});
                }
            })
        }
        else
        {
            res.status(401).json({msg: "password missing"});
        }

    });

    router.post('/authenticate', (req, res) => {
        const username = req.body.username;
        const password = req.body.password;

        if (!username || !password) {
            let msg = "Username or password missing!";
            console.error(msg);
            res.json({msg: msg});
            return;
        }

        User.findOne({ username: username}, (err, user) => {
            if (user) {
                bcrypt.compare(password, user.password, (err, result) => {
                    if (result) {
                        const payload = {
                            username: username,
                            admin: false
                        };
                        const token = jwt.sign(payload, process.env.JWT_SECRET, { expiresIn: '1h' });

                        res.json({
                            msg: 'User authenticated successfully',
                            token: token
                        });
                    }
                    else res.json({msg: "Password mismatch!"})
                });
            } else {
                res.json({msg: "User not found!"});
            }
        })
    });

    return router;
};