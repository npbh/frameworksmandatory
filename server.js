const express = require('express');
const bodyParser = require("body-parser");
const path = require('path');
const app = express();
const checkJwt = require('express-jwt');
const mongoose = require('mongoose');
const pathToRegexp = require('path-to-regexp');

mongoose.connect('mongodb://dbuser:niels2502@5.186.60.140/frameworksN?authSource=admin', {useNewUrlParser: true});
//mongoose.connect('mongodb://dbuser:niels2502@192.168.1.142/frameworksN?authSource=admin', {useNewUrlParser: true});

if (!process.env.JWT_SECRET) {
    console.error('You need to put a secret in the JWT_SECRET env variable!');
    process.exit(1);
}
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, 'client/build')));
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Authorization, Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");

    // intercepts OPTIONS method
    if ('OPTIONS' === req.method) {
        // respond with 200
        console.log("Allowing OPTIONS");
        res.sendStatus(200);
    } else {
        // move on
        next();
    }
});

let openPath = function(req) {
    if (req.path === '/api/users/authenticate')return true;
    else if(req.path === '/api/users' && req.method === "POST") return true;
    else if(req.method === "GET")
    {
        if(req.path === '/api/questions') return true;
        else if(pathToRegexp('/api/questions/:id').test(req.path)) return true;
        else if(req.path === '/') return true;
        else if(req.path === '/index.html') return true;
        else if(req.path === '/Login') return true;
        else if(req.path === '/AddQuestion') return true;
        else if(pathToRegexp('/Question/*').test(req.path)) return true;
    }
    return false;
}

// Validate the user using authentication
app.use(
    checkJwt({ secret: process.env.JWT_SECRET }).unless(openPath)
);
app.use((err, req, res, next) => {
    if (err.name === 'UnauthorizedError') {
        res.status(401).json({ error: err.message });
    }
});



const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    console.log("Connected")
});

let usersRouter = require('./usersRouter')(mongoose);
app.use('/api/users', usersRouter);

let questionRouter = require('./questionRouter')(mongoose);
app.use('/api/questions', questionRouter);

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname+'/client/build/index.html'));
});

const port = process.env.PORT || 5000;
app.listen(port);

console.log(`Serverlistening on ${port}`);